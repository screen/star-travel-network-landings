<div class="mailchimp-form-container">
    <form id="formSubscribe" class="mailchimp-form-container-wrapper">
        <div class="input-group">
            <input type="email" class="form-control form-email-input" placeholder="<?php _e("Escribe tu mejor email.", "startravel"); ?>" aria-label="<?php _e("Escribe tu mejor email.", "startravel"); ?>" aria-describedby="button-addon2" />
            <div class="input-group-append">
                <button id="formSubscribeSubmit" type="submit" class="btn btn-outline-secondary" type="button" id="button-addon2"><?php _e("¡Notificarme!", "startravel"); ?></button>
            </div>
        </div>
        <div id="errorEmail" class="error-thanks-email invalid-feedback d-none">
            <?php _e('El correo no es válido, vuelva a intentarlo', 'startravel'); ?>
        </div>
        <div class="loader-css d-none"></div>
    </form>
</div>