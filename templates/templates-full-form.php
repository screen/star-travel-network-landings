<form id="formModal" class="main-fullform-container">
    <input type="hidden" id="listID" name="listID" value="<?php echo get_post_meta(get_the_ID(), 'stn_modal_list_id', true); ?>" />
    <div class="fullform-input-container">
        <label for="inputName"><?php _e('Nombre', 'startravel'); ?></label>
        <input type="text" name="nombre" id="inputName" class="form-control" placeholder="<?php _e('Ingrese su nombre', 'startravel'); ?>" />
        <small class="error-name text-danger d-none"></small>
    </div>

    <div class="fullform-input-container">
        <label for="inputEmail"><?php _e('Correo Electrónico', 'startravel'); ?></label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="<?php _e('Ingrese su correo electrónico', 'startravel'); ?>" />
        <small class="error-email text-danger d-none"></small>
    </div>

    <div class="fullform-input-container">
        <button type="submit" id="formModalSubmit" class="btn btn-md btn-modal"><?php _e('¡Descargar Ahora!')?></button>
    </div>

    <div class="fullform-input-container">
        <div class="loader-css d-none"></div>
    </div>
</form>