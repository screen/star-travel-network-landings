<?php

/**
 * Template Name: Pagina Landing [Webinar]
 *
 * @package startravel
 * @subpackage startravel-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header('landing'); ?>
<?php the_post(); ?>
<main class="container-fluid container-webinar p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section class="main-hero-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="main-hero-content col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="main-hero-boxed-content">
                            <?php the_content(); ?>
                            <?php $link = get_post_meta(get_the_ID(), 'stn_hero_btn_link', true); ?>
                            <?php $msg = get_post_meta(get_the_ID(), 'stn_hero_btn_text', true); ?>
                            <a href="<?php echo $link; ?>" title="<?php echo $msg; ?>" class="btn btn-md btn-yellow btn-hero"><?php echo $msg; ?></a>
                            <div class="box-webinar-lines">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/frame.png" alt="Webinar Gratuito" class="img-fluid" />
                                <img src="<?php echo get_template_directory_uri(); ?>/images/webinar-letters.png" alt="Webinar Gratuito" class="img-fluid" />
                            </div>

                        </div>
                    </div>
                    <div class="main-hero-image-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div id="swiperHero" class="main-hero-image-swiper swiper-container">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <?php $arr_group = get_post_meta(get_the_ID(), 'stn_banners_list_group', true); ?>
                                <?php foreach ($arr_group as $item) { ?>
                                <div class="swiper-slide">
                                    <div class="main-hero-image-wrapper">
                                        <?php $bg_banner_id = $item['bg_person_id']; ?>
                                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                                        <img itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-person" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                        <?php /* BG IMAGE */ ?>
                                        <?php $bg_banner_id = $item['bg_image_id']; ?>
                                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                                        <img itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-background" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                        <div class="main-hero-image-info-box">
                                            <div class="star-container">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <h3><?php echo $item['name']; ?></h3>
                                            <?php echo apply_filters('the_content', $item['content']); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'stn_desc_bg_image_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <section class="main-ebook-description-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="main-ebook-description-content col-xl-9 col-lg-10 col-md-12 col-sm-12 col-12">
                        <div class="main-ebook-description-title">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'stn_desc_main_text', true)); ?>
                        </div>
                        <div class="main-ebook-description-text">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'stn_desc_second_text', true)); ?>
                        </div>
                        <?php $link = get_post_meta(get_the_ID(), 'stn_desc_btn_link', true); ?>
                        <?php $msg = get_post_meta(get_the_ID(), 'stn_desc_btn_text', true); ?>
                        <a href="<?php echo $link; ?>" title="<?php echo $msg; ?>" class="btn btn-md btn-yellow btn-hero"><?php echo $msg; ?></a>
                        <small><?php echo get_post_meta(get_the_ID(), 'stn_desc_post_btn_text', true); ?></small>
                    </div>
                </div>
            </div>
        </section>

        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'stn_know_bg_image_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <section class="main-know-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row">
                    <div class="main-know-content col-xl-8 offset-xl-4 col-lg-9 offset-lg-3 col-md-12 col-sm-12 col-12">
                        <div class="main-know-description-title">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'stn_know_main_text', true)); ?>
                        </div>
                        <div class="main-know-description-text">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'stn_know_second_text', true)); ?>
                        </div>
                        <?php $link = get_post_meta(get_the_ID(), 'stn_know_btn_link', true); ?>
                        <?php $msg = get_post_meta(get_the_ID(), 'stn_know_btn_text', true); ?>
                        <a href="<?php echo $link; ?>" title="<?php echo $msg; ?>" class="btn btn-md btn-yellow btn-hero"><?php echo $msg; ?></a>
                        <small><?php echo get_post_meta(get_the_ID(), 'stn_know_post_btn_text', true); ?></small>
                    </div>
                </div>
            </div>
        </section>

        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'stn_bullets_bg_image_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <section class="main-bullets-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="main-bullets-content col-xl-6 col-lg-7 col-md-10 col-sm-11 col-12">
                        <div class="main-bullets-title">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'stn_bullets_main_title', true)); ?>
                        </div>
                        <?php $arr_list = get_post_meta(get_the_ID(), 'stn_bullets_list_group', true); ?>
                        <ul class="main-bullets-list-content">
                            <?php foreach ($arr_list as $item) { ?>
                            <li>
                                <?php echo apply_filters('the_content', $item['content']); ?>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php $link = get_post_meta(get_the_ID(), 'stn_bullets_btn_link', true); ?>
                        <?php $msg = get_post_meta(get_the_ID(), 'stn_bullets_btn_text', true); ?>
                        <a href="<?php echo $link; ?>" title="<?php echo $msg; ?>" class="btn btn-md btn-yellow btn-hero"><?php echo $msg; ?></a>
                        <small><?php echo get_post_meta(get_the_ID(), 'stn_bullets_post_btn_text', true); ?></small>
                    </div>
                </div>
            </div>
        </section>

        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'stn_about_bg_image_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <section class="main-about-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row">
                    <div class="main-about-content col-xl-8 col-lg-9  col-md-12 col-sm-12 col-12">
                        <div class="main-about-title">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'stn_about_main_title', true)); ?>
                        </div>
                        <div class="main-about-subtitle">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'stn_about_second_title', true)); ?>
                        </div>
                        <div class="main-about-text">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'stn_about_main_text', true)); ?>
                        </div>
                    </div>
                    <div class="w-100"></div>
                </div>
            </div>
        </section>

        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'stn_bullets2_bg_image_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <section class="main-bullets-container bullets2 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="main-bullets-content col-xl-6 col-lg-7 col-md-10 col-sm-11 col-12">
                        <div class="main-bullets-title">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'stn_bullets2_main_title', true)); ?>
                        </div>
                        <?php $arr_list = get_post_meta(get_the_ID(), 'stn_bullets2_list_group', true); ?>
                        <ul class="main-bullets-list-content">
                            <?php foreach ($arr_list as $item) { ?>
                            <li>
                                <?php echo apply_filters('the_content', $item['content']); ?>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php $link = get_post_meta(get_the_ID(), 'stn_bullets2_btn_link', true); ?>
                        <?php $msg = get_post_meta(get_the_ID(), 'stn_bullets2_btn_text', true); ?>
                        <a href="<?php echo $link; ?>" title="<?php echo $msg; ?>" class="btn btn-md btn-yellow btn-hero"><?php echo $msg; ?></a>
                        <small><?php echo get_post_meta(get_the_ID(), 'stn_bullets2_post_btn_text', true); ?></small>
                    </div>
                </div>
            </div>
        </section>

        <section class="main-services-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-start">
                    <div class="main-services-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="main-services-title">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'stn_services_main_title', true)); ?>
                        </div>
                        <div class="main-services-subtitle">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'stn_services_subtitle', true)); ?>
                        </div>
                    </div>
                    <?php $arr_list = get_post_meta(get_the_ID(), 'stn_services_list_group', true); ?>
                    <?php foreach ($arr_list as $item) { ?>
                    <div class="services-item col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="services-item-wrapper">
                            <?php $bg_banner_id = $item['icon_id']; ?>
                            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                            <img itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                            <h3><?php echo $item['title']; ?></h3>
                            <div class="services-item-wrapper-text">
                                <?php echo apply_filters('the_content', $item['content']); ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="main-services-button-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $link = get_post_meta(get_the_ID(), 'stn_services_btn_link', true); ?>
                        <?php $msg = get_post_meta(get_the_ID(), 'stn_services_btn_text', true); ?>
                        <a href="<?php echo $link; ?>" title="<?php echo $msg; ?>" class="btn btn-md btn-yellow btn-hero"><?php echo $msg; ?></a>
                        <small><?php echo get_post_meta(get_the_ID(), 'stn_services_post_btn_text', true); ?></small>
                    </div>
                </div>
            </div>
        </section>

        <section class="main-test-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-start">
                    <div class="main-test-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="main-test-title">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'stn_test_main_title', true)); ?>
                        </div>
                        <div class="main-test-slider-container">

                        </div>
                        <div class="main-test-text">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'stn_test_subtext', true)); ?>
                        </div>
                        <?php $link = get_post_meta(get_the_ID(), 'stn_test_btn_link', true); ?>
                        <?php $msg = get_post_meta(get_the_ID(), 'stn_test_btn_text', true); ?>
                        <a href="<?php echo $link; ?>" title="<?php echo $msg; ?>" class="btn btn-md btn-yellow btn-hero"><?php echo $msg; ?></a>
                        <small><?php echo get_post_meta(get_the_ID(), 'stn_test_post_btn_text', true); ?></small>
                    </div>
                </div>
            </div>
        </section>

        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'stn_faq_bg_image_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <section class="main-faq-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row align-items-start">
                    <div class="main-faq-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="main-faq-title">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'stn_faq_main_title', true)); ?>
                        </div>
                        <div class="main-faq-subtitle">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'stn_faq_main_subtitle', true)); ?>
                        </div>
                        <div class="main-faq-collapse-container">
                            <div class="accordion" id="faqAccordion">
                                <?php $arr_faq = get_post_meta(get_the_ID(), 'stn_faq_list_group', true);
                                $i = 1; ?>
                                <?php foreach ($arr_faq as $item) { ?>
                                <div class="card">
                                    <div class="card-header" id="heading<?php echo $i; ?>">
                                        <h2 class="mb-0">
                                            <?php $class = ($i > 1) ? 'collapsed' : ''; ?>
                                            <button class="btn btn-link btn-collapse <?php echo $class; ?>" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="true" aria-controls="collapse<?php echo $i; ?>">
                                                <?php echo $item['title']; ?>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/chevron.png" alt="Chevron" class="img-chevron"></span>
                                            </button>
                                        </h2>
                                    </div>
                                    <?php $class = ($i == 1) ? 'show' : ''; ?>
                                    <div id="collapse<?php echo $i; ?>" class="collapse <?php echo $class; ?>" aria-labelledby="heading<?php echo $i; ?>" data-parent="#faqAccordion">
                                        <div class="card-body">
                                            <?php echo apply_filters('the_content', $item['content']); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++;
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</main>
<?php get_footer(); ?>