<footer class="container-fluid p-0" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row no-gutters">
        <div class="footer-copy footer-empty col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <div class="footer-copy-left col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                        <div class="media-custom">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon.png" alt="Star Travel Network" class="img-fluid" />
                            <div class="media-body">
                            <p>&copy; <?php _e('Star Travel Network', 'startravel'); ?>. 2020</p>
                            <p class="text-uppercase"><?php _e('Desarrollado por', 'startravel'); ?> <a href="https://screenmediagroup.com/" target="_blank" title="SMG | Digital Marketing Agency">SMG | Digital Marketing Agency</a></p>
                            </div>
                        </div>
                    </div>
                    <?php /* ?>
                    <div class="footer-copy-right col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <a href=""><i class="fa fa-linkedin"></i></a>
                        <a href=""><i class="fa fa-instagram"></i></a>
                        <a href=""><i class="fa fa-youtube-play"></i></a>
                        <a href=""><i class="fa fa-pinterest"></i></a>
                    </div>
                    <?php */ ?>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>

</html>