<?php
/*
function startravel_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'startravel' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'startravel' ),
		'menu_name'             => __( 'Clientes', 'startravel' ),
		'name_admin_bar'        => __( 'Clientes', 'startravel' ),
		'archives'              => __( 'Archivo de Clientes', 'startravel' ),
		'attributes'            => __( 'Atributos de Cliente', 'startravel' ),
		'parent_item_colon'     => __( 'Cliente Padre:', 'startravel' ),
		'all_items'             => __( 'Todos los Clientes', 'startravel' ),
		'add_new_item'          => __( 'Agregar Nuevo Cliente', 'startravel' ),
		'add_new'               => __( 'Agregar Nuevo', 'startravel' ),
		'new_item'              => __( 'Nuevo Cliente', 'startravel' ),
		'edit_item'             => __( 'Editar Cliente', 'startravel' ),
		'update_item'           => __( 'Actualizar Cliente', 'startravel' ),
		'view_item'             => __( 'Ver Cliente', 'startravel' ),
		'view_items'            => __( 'Ver Clientes', 'startravel' ),
		'search_items'          => __( 'Buscar Cliente', 'startravel' ),
		'not_found'             => __( 'No hay resultados', 'startravel' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'startravel' ),
		'featured_image'        => __( 'Imagen del Cliente', 'startravel' ),
		'set_featured_image'    => __( 'Colocar Imagen del Cliente', 'startravel' ),
		'remove_featured_image' => __( 'Remover Imagen del Cliente', 'startravel' ),
		'use_featured_image'    => __( 'Usar como Imagen del Cliente', 'startravel' ),
		'insert_into_item'      => __( 'Insertar en Cliente', 'startravel' ),
		'uploaded_to_this_item' => __( 'Cargado a este Cliente', 'startravel' ),
		'items_list'            => __( 'Listado de Clientes', 'startravel' ),
		'items_list_navigation' => __( 'Navegación del Listado de Cliente', 'startravel' ),
		'filter_items_list'     => __( 'Filtro del Listado de Clientes', 'startravel' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'startravel' ),
		'description'           => __( 'Portafolio de Clientes', 'startravel' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'tipos-de-clientes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 15,
		'menu_icon'             => 'dashicons-businessperson',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'clientes', $args );

}

add_action( 'init', 'startravel_custom_post_type', 0 );
*/
