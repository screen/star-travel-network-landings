<?php
/* --------------------------------------------------------------
    1.- MAIN HERO
-------------------------------------------------------------- */
$cmb_main_hero_webinar = new_cmb2_box( array(
    'id'            => $prefix . 'main_hero_webinar_metabox',
    'title'         => esc_html__( '1.- Hero Principal', 'tevard' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-webinar.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$group_field_id = $cmb_main_hero_webinar->add_field( array(
    'id'          => $prefix . 'banners_list_group',
    'name'       => esc_html__( 'Listado de Personas en Banner', 'tevard' ),
    'description' => __( 'Gropo de items en lista no ordenada', 'tevard' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Item {#}', 'tevard' ),
        'add_button'        => __( 'Agregar otro item', 'tevard' ),
        'remove_button'     => __( 'Remover Item', 'tevard' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro de remover este item?', 'tevard' )
    )
) );

$cmb_main_hero_webinar->add_group_field( $group_field_id, array(
    'id'         => 'bg_image',
    'name'      => esc_html__( 'Fondo del Banner', 'tevard' ),
    'desc'      => esc_html__( 'Cargue un fondo azul para este banner', 'tevard' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'tevard' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_main_hero_webinar->add_group_field( $group_field_id, array(
    'id'         => 'bg_person',
    'name'      => esc_html__( 'Persona del Banner', 'tevard' ),
    'desc'      => esc_html__( 'Cargue una imagen de la persona para este banner', 'tevard' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'tevard' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_main_hero_webinar->add_group_field( $group_field_id, array(
    'id'        => 'name',
    'name'      => esc_html__( 'Nombre de la Persona', 'tevard' ),
    'desc'      => esc_html__( 'Ingrese el nombre para este item', 'tevard' ),
    'type' => 'text'
) );

$cmb_main_hero_webinar->add_group_field( $group_field_id, array(
    'id'        => 'content',
    'name'      => esc_html__( 'Cargo del Item', 'tevard' ),
    'desc'      => esc_html__( 'Ingrese el cargo para este item', 'tevard' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$cmb_main_hero_webinar->add_field( array(
    'name'       => esc_html__( 'Texto del Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un texto para el Boton Principal', 'tevard' ),
    'id'         => $prefix . 'hero_btn_text',
    'type'       => 'text'
) );

$cmb_main_hero_webinar->add_field( array(
    'name'       => esc_html__( 'Link URL del Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un link o una URL para el Boton Principal', 'tevard' ),
    'id'         => $prefix . 'hero_btn_link',
    'type'       => 'text_url'
) );

/* --------------------------------------------------------------
    2.- webinar DESCRIPTION
-------------------------------------------------------------- */
$cmb_webinar_desc = new_cmb2_box( array(
    'id'            => $prefix . 'main_webinar_desc_metabox',
    'title'         => esc_html__( '2.- Descripción del e-Book', 'tevard' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-webinar.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_webinar_desc->add_field( array(
    'name'      => esc_html__( 'Imagen de Fondo de la Sección', 'tevard' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para esta sección', 'tevard' ),
    'id'         => $prefix . 'desc_bg_image',
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'tevard' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_webinar_desc->add_field( array(
    'name'       => esc_html__( 'Texto Principal', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese el texto Principal de esta sección', 'tevard' ),
    'id'         => $prefix . 'desc_main_text',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_webinar_desc->add_field( array(
    'name'       => esc_html__( 'Texto Secundario', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese el Texto Secundario de esta sección', 'tevard' ),
    'id'         => $prefix . 'desc_second_text',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_webinar_desc->add_field( array(
    'name'       => esc_html__( 'Texto del Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un texto para el Boton Principal', 'tevard' ),
    'id'         => $prefix . 'desc_btn_text',
    'type'       => 'text'
) );

$cmb_webinar_desc->add_field( array(
    'name'       => esc_html__( 'Link URL del Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un link o una URL para el Boton Principal', 'tevard' ),
    'id'         => $prefix . 'desc_btn_link',
    'type'       => 'text_url'
) );

$cmb_webinar_desc->add_field( array(
    'name'       => esc_html__( 'Texto Posterior al Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un texto ubicado luego del Boton Principal', 'tevard' ),
    'id'         => $prefix . 'desc_post_btn_text',
    'type'       => 'text'
) );

/* --------------------------------------------------------------
    3.- DESCANSO - ¿SABIAS QUE?
-------------------------------------------------------------- */
$cmb_hero_know_webinar = new_cmb2_box( array(
    'id'            => $prefix . 'main_know_hero_webinar_metabox',
    'title'         => esc_html__( '3.- Descanso - ¿Sabías que?', 'tevard' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-webinar.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_hero_know_webinar->add_field( array(
    'name'      => esc_html__( 'Imagen de Fondo de la Sección', 'tevard' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para esta sección', 'tevard' ),
    'id'         => $prefix . 'know_bg_image',
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'tevard' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_hero_know_webinar->add_field( array(
    'name'       => esc_html__( 'Texto Principal', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese el texto Principal de esta sección', 'tevard' ),
    'id'         => $prefix . 'know_main_text',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_hero_know_webinar->add_field( array(
    'name'       => esc_html__( 'Texto Secundario', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese el Texto Secundario de esta sección', 'tevard' ),
    'id'         => $prefix . 'know_second_text',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_hero_know_webinar->add_field( array(
    'name'       => esc_html__( 'Texto del Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un texto para el Boton Principal', 'tevard' ),
    'id'         => $prefix . 'know_btn_text',
    'type'       => 'text'
) );

$cmb_hero_know_webinar->add_field( array(
    'name'       => esc_html__( 'Link URL del Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un link o una URL para el Boton Principal', 'tevard' ),
    'id'         => $prefix . 'know_btn_link',
    'type'       => 'text_url'
) );

$cmb_hero_know_webinar->add_field( array(
    'name'       => esc_html__( 'Texto Posterior al Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un texto ubicado luego del Boton Principal', 'tevard' ),
    'id'         => $prefix . 'know_post_btn_text',
    'type'       => 'text'
) );

/* --------------------------------------------------------------
    4.- DESCANSO - BULLETS SECTION
-------------------------------------------------------------- */
$cmb_hero_bullets_webinar = new_cmb2_box( array(
    'id'            => $prefix . 'main_hero_bullets_webinar_metabox',
    'title'         => esc_html__( '4.- Descanso - Sección de Listas', 'tevard' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-webinar.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_hero_bullets_webinar->add_field( array(
    'name'      => esc_html__( 'Imagen de Fondo de la Sección', 'tevard' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para esta sección', 'tevard' ),
    'id'         => $prefix . 'bullets_bg_image',
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'tevard' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_hero_bullets_webinar->add_field( array(
    'name'       => esc_html__( 'Título de la Sección', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un título descriptivo para esta sección', 'tevard' ),
    'id'         => $prefix . 'bullets_main_title',
    'type'       => 'text'
) );

$group_field_id = $cmb_hero_bullets_webinar->add_field( array(
    'id'          => $prefix . 'bullets_list_group',
    'name'       => esc_html__( 'Listado de Items', 'tevard' ),
    'description' => __( 'Gropo de items en lista no ordenada', 'tevard' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Item {#}', 'tevard' ),
        'add_button'        => __( 'Agregar otro item', 'tevard' ),
        'remove_button'     => __( 'Remover Item', 'tevard' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro de remover este item?', 'tevard' )
    )
) );

$cmb_hero_bullets_webinar->add_group_field( $group_field_id, array(
    'id'        => 'content',
    'name'      => esc_html__( 'Contenido del Item', 'tevard' ),
    'desc'      => esc_html__( 'Ingrese el contenido para este item', 'tevard' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$cmb_hero_bullets_webinar->add_field( array(
    'name'       => esc_html__( 'Texto del Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un texto para el Boton Principal', 'tevard' ),
    'id'         => $prefix . 'bullets_btn_text',
    'type'       => 'text'
) );

$cmb_hero_bullets_webinar->add_field( array(
    'name'       => esc_html__( 'Link URL del Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un link o una URL para el Boton Principal', 'tevard' ),
    'id'         => $prefix . 'bullets_btn_link',
    'type'       => 'text_url'
) );

$cmb_hero_bullets_webinar->add_field( array(
    'name'       => esc_html__( 'Texto Posterior al Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un texto ubicado luego del Boton Principal', 'tevard' ),
    'id'         => $prefix . 'bullets_post_btn_text',
    'type'       => 'text'
) );

/* --------------------------------------------------------------
    5.- DESCANSO - ABOUT - PERSPECTIVA
-------------------------------------------------------------- */
$cmb_hero_about_webinar = new_cmb2_box( array(
    'id'            => $prefix . 'main_hero_about_webinar_metabox',
    'title'         => esc_html__( '5.- Descanso - Perspectiva', 'tevard' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-webinar.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_hero_about_webinar->add_field( array(
    'name'      => esc_html__( 'Imagen de Fondo de la Sección', 'tevard' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para esta sección', 'tevard' ),
    'id'         => $prefix . 'about_bg_image',
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'tevard' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_hero_about_webinar->add_field( array(
    'name'       => esc_html__( 'Título Principal', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un título descriptivo para esta sección', 'tevard' ),
    'id'         => $prefix . 'about_main_title',
    'type'       => 'text'
) );

$cmb_hero_about_webinar->add_field( array(
    'name'       => esc_html__( 'Título Secundario', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un segundo título descriptivo para esta sección', 'tevard' ),
    'id'         => $prefix . 'about_second_title',
    'type'       => 'text'
) );

$cmb_hero_about_webinar->add_field( array(
    'name'       => esc_html__( 'Texto Principal', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese el texto Principal de esta sección', 'tevard' ),
    'id'         => $prefix . 'about_main_text',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

/* --------------------------------------------------------------
    6.- DESCANSO - BULLETS SECTION
-------------------------------------------------------------- */
$cmb_hero_bullets2_webinar = new_cmb2_box( array(
    'id'            => $prefix . 'main_hero_bullets2_webinar_metabox',
    'title'         => esc_html__( '6.- Descanso - Sección de Listas "Oportunidad"', 'tevard' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-webinar.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_hero_bullets2_webinar->add_field( array(
    'name'      => esc_html__( 'Imagen de Fondo de la Sección', 'tevard' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para esta sección', 'tevard' ),
    'id'         => $prefix . 'bullets2_bg_image',
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'tevard' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_hero_bullets2_webinar->add_field( array(
    'name'       => esc_html__( 'Título de la Sección', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un título descriptivo para esta sección', 'tevard' ),
    'id'         => $prefix . 'bullets2_main_title',
    'type'       => 'text'
) );

$group_field_id = $cmb_hero_bullets2_webinar->add_field( array(
    'id'          => $prefix . 'bullets2_list_group',
    'name'       => esc_html__( 'Listado de Items', 'tevard' ),
    'description' => __( 'Gropo de items en lista no ordenada', 'tevard' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Item {#}', 'tevard' ),
        'add_button'        => __( 'Agregar otro item', 'tevard' ),
        'remove_button'     => __( 'Remover Item', 'tevard' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro de remover este item?', 'tevard' )
    )
) );

$cmb_hero_bullets2_webinar->add_group_field( $group_field_id, array(
    'id'        => 'content',
    'name'      => esc_html__( 'Contenido del Item', 'tevard' ),
    'desc'      => esc_html__( 'Ingrese el contenido para este item', 'tevard' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$cmb_hero_bullets2_webinar->add_field( array(
    'name'       => esc_html__( 'Texto del Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un texto para el Boton Principal', 'tevard' ),
    'id'         => $prefix . 'bullets2_btn_text',
    'type'       => 'text'
) );

$cmb_hero_bullets2_webinar->add_field( array(
    'name'       => esc_html__( 'Link URL del Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un link o una URL para el Boton Principal', 'tevard' ),
    'id'         => $prefix . 'bullets2_btn_link',
    'type'       => 'text_url'
) );

$cmb_hero_bullets2_webinar->add_field( array(
    'name'       => esc_html__( 'Texto Posterior al Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un texto ubicado luego del Boton Principal', 'tevard' ),
    'id'         => $prefix . 'bullets2_post_btn_text',
    'type'       => 'text'
) );

/* --------------------------------------------------------------
    7.- SERVICIOS
-------------------------------------------------------------- */
$cmb_hero_services_webinar = new_cmb2_box( array(
    'id'            => $prefix . 'main_landing_services_webinar_metabox',
    'title'         => esc_html__( '7.- Servicios', 'tevard' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-webinar.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_hero_services_webinar->add_field( array(
    'name'       => esc_html__( 'Título de la Sección', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un título descriptivo para esta sección', 'tevard' ),
    'id'         => $prefix . 'services_main_title',
    'type'       => 'text'
) );

$cmb_hero_services_webinar->add_field( array(
    'name'       => esc_html__( 'Subtítulo de la Sección', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un subtítulo descriptivo para esta sección', 'tevard' ),
    'id'         => $prefix . 'services_subtitle',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$group_field_id = $cmb_hero_services_webinar->add_field( array(
    'id'          => $prefix . 'services_list_group',
    'name'       => esc_html__( 'Listado de Servicios', 'tevard' ),
    'description' => __( 'Gropo de Servicios en lista no ordenada', 'tevard' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Servicio {#}', 'tevard' ),
        'add_button'        => __( 'Agregar otro Servicio', 'tevard' ),
        'remove_button'     => __( 'Remover Servicio', 'tevard' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro de remover este Servicio?', 'tevard' )
    )
) );

$cmb_hero_services_webinar->add_group_field( $group_field_id, array(
    'id'         => 'icon',
    'name'      => esc_html__( 'Ícono del Servicio', 'tevard' ),
    'desc'      => esc_html__( 'Cargue un Ícono para este servicio', 'tevard' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Ícono', 'tevard' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_hero_services_webinar->add_group_field( $group_field_id, array(
    'id'         => 'title',
    'name'       => esc_html__( 'Titulo del Servicio', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un Titulo para el Boton Principal', 'tevard' ),
    'type'       => 'text'
) );

$cmb_hero_services_webinar->add_group_field( $group_field_id, array(
    'id'        => 'content',
    'name'      => esc_html__( 'Contenido del Servicio', 'tevard' ),
    'desc'      => esc_html__( 'Ingrese el contenido para este Servicio', 'tevard' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$cmb_hero_services_webinar->add_field( array(
    'name'       => esc_html__( 'Texto del Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un texto para el Boton Principal', 'tevard' ),
    'id'         => $prefix . 'services_btn_text',
    'type'       => 'text'
) );

$cmb_hero_services_webinar->add_field( array(
    'name'       => esc_html__( 'Link URL del Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un link o una URL para el Boton Principal', 'tevard' ),
    'id'         => $prefix . 'services_btn_link',
    'type'       => 'text_url'
) );

$cmb_hero_services_webinar->add_field( array(
    'name'       => esc_html__( 'Texto Posterior al Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un texto ubicado luego del Boton Principal', 'tevard' ),
    'id'         => $prefix . 'services_post_btn_text',
    'type'       => 'text'
) );

/* --------------------------------------------------------------
    8.- TESTIMONIOS
-------------------------------------------------------------- */
$cmb_hero_test_webinar = new_cmb2_box( array(
    'id'            => $prefix . 'main_landing_test_webinar_metabox',
    'title'         => esc_html__( '8.- Testimonios', 'tevard' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-webinar.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_hero_test_webinar->add_field( array(
    'name'       => esc_html__( 'Título de la Sección', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un título descriptivo para esta sección', 'tevard' ),
    'id'         => $prefix . 'test_main_title',
    'type'       => 'text'
) );

$cmb_hero_test_webinar->add_field( array(
    'name'       => esc_html__( 'Subtexto de la Sección', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un subtexto descriptivo para esta sección', 'tevard' ),
    'id'         => $prefix . 'test_subtext',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$cmb_hero_test_webinar->add_field( array(
    'name'       => esc_html__( 'Texto del Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un texto para el Boton Principal', 'tevard' ),
    'id'         => $prefix . 'test_btn_text',
    'type'       => 'text'
) );

$cmb_hero_test_webinar->add_field( array(
    'name'       => esc_html__( 'Link URL del Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un link o una URL para el Boton Principal', 'tevard' ),
    'id'         => $prefix . 'test_btn_link',
    'type'       => 'text_url'
) );

$cmb_hero_test_webinar->add_field( array(
    'name'       => esc_html__( 'Texto Posterior al Botón', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un texto ubicado luego del Boton Principal', 'tevard' ),
    'id'         => $prefix . 'test_post_btn_text',
    'type'       => 'text'
) );

/* --------------------------------------------------------------
    9.- FAQ
-------------------------------------------------------------- */
$cmb_hero_faq_webinar = new_cmb2_box( array(
    'id'            => $prefix . 'main_landing_faq_webinar_metabox',
    'title'         => esc_html__( '9.- Preguntas Frecuentes', 'tevard' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-webinar.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_hero_faq_webinar->add_field( array(
    'name'      => esc_html__( 'Imagen de Fondo de la Sección', 'tevard' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para esta sección', 'tevard' ),
    'id'         => $prefix . 'faq_bg_image',
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'tevard' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_hero_faq_webinar->add_field( array(
    'name'       => esc_html__( 'Título de la Sección', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un título descriptivo para esta sección', 'tevard' ),
    'id'         => $prefix . 'faq_main_title',
    'type'       => 'text'
) );

$cmb_hero_faq_webinar->add_field( array(
    'name'       => esc_html__( 'Subtexto de la Sección', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un subtexto descriptivo para esta sección', 'tevard' ),
    'id'         => $prefix . 'faq_main_subtitle',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$group_field_id = $cmb_hero_faq_webinar->add_field( array(
    'id'          => $prefix . 'faq_list_group',
    'name'       => esc_html__( 'Listado de Preguntas', 'tevard' ),
    'description' => __( 'Gropo de Preguntas en lista no ordenada', 'tevard' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Pregunta {#}', 'tevard' ),
        'add_button'        => __( 'Agregar otra Pregunta', 'tevard' ),
        'remove_button'     => __( 'Remover Pregunta', 'tevard' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro de remover esta Pregunta?', 'tevard' )
    )
) );

$cmb_hero_faq_webinar->add_group_field( $group_field_id, array(
    'id'         => 'title',
    'name'       => esc_html__( 'Titulo de la Pregunta', 'tevard' ),
    'desc'       => esc_html__( 'Ingrese un Titulo para esta Pregunta', 'tevard' ),
    'type'       => 'text'
) );

$cmb_hero_faq_webinar->add_group_field( $group_field_id, array(
    'id'        => 'content',
    'name'      => esc_html__( 'Contenido de la Pregunta', 'tevard' ),
    'desc'      => esc_html__( 'Ingrese el contenido para esta Pregunta', 'tevard' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

