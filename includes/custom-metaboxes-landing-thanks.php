<?php
/* --------------------------------------------------------------
    1.- PRELAUNCH OPTIONS
-------------------------------------------------------------- */
$cmb_thanks_metabox = new_cmb2_box(array(
    'id'            => $prefix . 'thanks_metabox',
    'title'         => esc_html__('Pre Launch: Información Extra', 'startravel'),
    'object_types'  => array('page'),
    'show_on'      => array('key' => 'page-template', 'value' => 'templates/page-landing-thanks.php'),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
));

$cmb_thanks_metabox->add_field(array(
    'id'   => $prefix . 'prelaunch_activate',
    'name'      => esc_html__('Activar campos para páagina de prelaunch', 'startravel'),
    'desc'      => esc_html__('Active este check si necesita agregar un contador de tiempo y un formulario de subscripción', 'startravel'),
    'type' => 'checkbox'
));

$cmb_thanks_metabox->add_field(array(
    'id'   => $prefix . 'coming_date',
    'name'      => esc_html__('Fecha tope', 'startravel'),
    'desc'      => esc_html__('Ingresa la fecha tope para el PreLaunch', 'startravel'),
    'type' => 'text_date'
));


$cmb_thanks_metabox->add_field(array(
    'id'   => $prefix . 'thanks_button_subtitle',
    'name'      => esc_html__('Subtítulo anterior al boton de acción', 'startravel'),
    'desc'      => esc_html__('Ingresa un sobretitulo que estara arriba del boton de acción', 'startravel'),
    'type' => 'text'
));

$cmb_thanks_metabox->add_field(array(
    'id'   => $prefix . 'thanks_button_text',
    'name'      => esc_html__('Texto del boton de acción', 'startravel'),
    'desc'      => esc_html__('Ingresa un texto descriptivo para el boton de acción', 'startravel'),
    'type' => 'text'
));

$cmb_thanks_metabox->add_field(array(
    'id'   => $prefix . 'thanks_button_link',
    'name'      => esc_html__('Link URL del boton de acción', 'startravel'),
    'desc'      => esc_html__('Ingresa un link descriptivo para el boton de acción', 'startravel'),
    'type' => 'text_url'
));