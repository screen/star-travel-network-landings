<?php
/* --------------------------------------------------------------
    1.- MAIN HERO
-------------------------------------------------------------- */
$cmb_main_hero = new_cmb2_box( array(
    'id'            => $prefix . 'main_hero_metabox',
    'title'         => esc_html__( '1.- Hero Principal', 'startravel' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-ebook.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_main_hero->add_field( array(
    'name'      => esc_html__( 'Imagen Principal del Hero', 'startravel' ),
    'desc'      => esc_html__( 'Cargue una imagen descriptiva para el Hero', 'startravel' ),
    'id'         => $prefix . 'hero_main_img',
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'startravel' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_main_hero->add_field( array(
    'name'      => esc_html__( 'Fondo Principal del Hero', 'startravel' ),
    'desc'      => esc_html__( 'Cargue una fondo descritivo para el Hero', 'startravel' ),
    'id'         => $prefix . 'hero_main_bg',
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'startravel' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_main_hero->add_field( array(
    'name'       => esc_html__( 'Texto del Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un texto para el Boton Principal', 'startravel' ),
    'id'         => $prefix . 'hero_btn_text',
    'type'       => 'text'
) );

$cmb_main_hero->add_field( array(
    'name'       => esc_html__( 'Link URL del Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un link o una URL para el Boton Principal', 'startravel' ),
    'id'         => $prefix . 'hero_btn_link',
    'type'       => 'text_url'
) );

/* --------------------------------------------------------------
    2.- EBOOK DESCRIPTION
-------------------------------------------------------------- */
$cmb_ebook_desc = new_cmb2_box( array(
    'id'            => $prefix . 'main_ebook_desc_metabox',
    'title'         => esc_html__( '2.- Descripción del e-Book', 'startravel' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-ebook.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_ebook_desc->add_field( array(
    'name'      => esc_html__( 'Imagen de Fondo de la Sección', 'startravel' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para esta sección', 'startravel' ),
    'id'         => $prefix . 'desc_bg_image',
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'startravel' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_ebook_desc->add_field( array(
    'name'       => esc_html__( 'Texto Principal', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese el texto Principal de esta sección', 'startravel' ),
    'id'         => $prefix . 'desc_main_text',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_ebook_desc->add_field( array(
    'name'       => esc_html__( 'Texto Secundario', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese el Texto Secundario de esta sección', 'startravel' ),
    'id'         => $prefix . 'desc_second_text',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_ebook_desc->add_field( array(
    'name'       => esc_html__( 'Texto del Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un texto para el Boton Principal', 'startravel' ),
    'id'         => $prefix . 'desc_btn_text',
    'type'       => 'text'
) );

$cmb_ebook_desc->add_field( array(
    'name'       => esc_html__( 'Link URL del Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un link o una URL para el Boton Principal', 'startravel' ),
    'id'         => $prefix . 'desc_btn_link',
    'type'       => 'text_url'
) );

$cmb_ebook_desc->add_field( array(
    'name'       => esc_html__( 'Texto Posterior al Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un texto ubicado luego del Boton Principal', 'startravel' ),
    'id'         => $prefix . 'desc_post_btn_text',
    'type'       => 'text'
) );

/* --------------------------------------------------------------
    3.- DESCANSO - ¿SABIAS QUE?
-------------------------------------------------------------- */
$cmb_hero_know= new_cmb2_box( array(
    'id'            => $prefix . 'main_know_hero_metabox',
    'title'         => esc_html__( '3.- Descanso - ¿Sabías que?', 'startravel' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-ebook.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_hero_know->add_field( array(
    'name'      => esc_html__( 'Imagen de Fondo de la Sección', 'startravel' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para esta sección', 'startravel' ),
    'id'         => $prefix . 'know_bg_image',
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'startravel' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_hero_know->add_field( array(
    'name'       => esc_html__( 'Texto Principal', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese el texto Principal de esta sección', 'startravel' ),
    'id'         => $prefix . 'know_main_text',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_hero_know->add_field( array(
    'name'       => esc_html__( 'Texto Secundario', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese el Texto Secundario de esta sección', 'startravel' ),
    'id'         => $prefix . 'know_second_text',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_hero_know->add_field( array(
    'name'       => esc_html__( 'Texto del Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un texto para el Boton Principal', 'startravel' ),
    'id'         => $prefix . 'know_btn_text',
    'type'       => 'text'
) );

$cmb_hero_know->add_field( array(
    'name'       => esc_html__( 'Link URL del Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un link o una URL para el Boton Principal', 'startravel' ),
    'id'         => $prefix . 'know_btn_link',
    'type'       => 'text_url'
) );

$cmb_hero_know->add_field( array(
    'name'       => esc_html__( 'Texto Posterior al Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un texto ubicado luego del Boton Principal', 'startravel' ),
    'id'         => $prefix . 'know_post_btn_text',
    'type'       => 'text'
) );

/* --------------------------------------------------------------
    4.- DESCANSO - BULLETS SECTION
-------------------------------------------------------------- */
$cmb_hero_bullets= new_cmb2_box( array(
    'id'            => $prefix . 'main_hero_bullets_metabox',
    'title'         => esc_html__( '4.- Descanso - Sección de Listas', 'startravel' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-ebook.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_hero_bullets->add_field( array(
    'name'      => esc_html__( 'Imagen de Fondo de la Sección', 'startravel' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para esta sección', 'startravel' ),
    'id'         => $prefix . 'bullets_bg_image',
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'startravel' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_hero_bullets->add_field( array(
    'name'       => esc_html__( 'Título de la Sección', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un título descriptivo para esta sección', 'startravel' ),
    'id'         => $prefix . 'bullets_main_title',
    'type'       => 'text'
) );

$group_field_id = $cmb_hero_bullets->add_field( array(
    'id'          => $prefix . 'bullets_list_group',
    'name'       => esc_html__( 'Listado de Items', 'startravel' ),
    'description' => __( 'Gropo de items en lista no ordenada', 'startravel' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Item {#}', 'startravel' ),
        'add_button'        => __( 'Agregar otro item', 'startravel' ),
        'remove_button'     => __( 'Remover Item', 'startravel' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro de remover este item?', 'startravel' )
    )
) );

$cmb_hero_bullets->add_group_field( $group_field_id, array(
    'id'        => 'content',
    'name'      => esc_html__( 'Contenido del Item', 'startravel' ),
    'desc'      => esc_html__( 'Ingrese el contenido para este item', 'startravel' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$cmb_hero_bullets->add_field( array(
    'name'       => esc_html__( 'Texto del Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un texto para el Boton Principal', 'startravel' ),
    'id'         => $prefix . 'bullets_btn_text',
    'type'       => 'text'
) );

$cmb_hero_bullets->add_field( array(
    'name'       => esc_html__( 'Link URL del Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un link o una URL para el Boton Principal', 'startravel' ),
    'id'         => $prefix . 'bullets_btn_link',
    'type'       => 'text_url'
) );

$cmb_hero_bullets->add_field( array(
    'name'       => esc_html__( 'Texto Posterior al Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un texto ubicado luego del Boton Principal', 'startravel' ),
    'id'         => $prefix . 'bullets_post_btn_text',
    'type'       => 'text'
) );

/* --------------------------------------------------------------
    5.- DESCANSO - ABOUT - PERSPECTIVA
-------------------------------------------------------------- */
$cmb_hero_about = new_cmb2_box( array(
    'id'            => $prefix . 'main_hero_about_metabox',
    'title'         => esc_html__( '5.- Descanso - Perspectiva', 'startravel' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-ebook.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_hero_about->add_field( array(
    'name'      => esc_html__( 'Imagen de Fondo de la Sección', 'startravel' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para esta sección', 'startravel' ),
    'id'         => $prefix . 'about_bg_image',
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'startravel' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_hero_about->add_field( array(
    'name'       => esc_html__( 'Título Principal', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un título descriptivo para esta sección', 'startravel' ),
    'id'         => $prefix . 'about_main_title',
    'type'       => 'text'
) );

$cmb_hero_about->add_field( array(
    'name'       => esc_html__( 'Título Secundario', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un segundo título descriptivo para esta sección', 'startravel' ),
    'id'         => $prefix . 'about_second_title',
    'type'       => 'text'
) );

$cmb_hero_about->add_field( array(
    'name'       => esc_html__( 'Texto Principal', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese el texto Principal de esta sección', 'startravel' ),
    'id'         => $prefix . 'about_main_text',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

/* --------------------------------------------------------------
    6.- DESCANSO - BULLETS SECTION
-------------------------------------------------------------- */
$cmb_hero_bullets2 = new_cmb2_box( array(
    'id'            => $prefix . 'main_hero_bullets2_metabox',
    'title'         => esc_html__( '6.- Descanso - Sección de Listas "Oportunidad"', 'startravel' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-ebook.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_hero_bullets2->add_field( array(
    'name'      => esc_html__( 'Imagen de Fondo de la Sección', 'startravel' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para esta sección', 'startravel' ),
    'id'         => $prefix . 'bullets2_bg_image',
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'startravel' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_hero_bullets2->add_field( array(
    'name'       => esc_html__( 'Título de la Sección', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un título descriptivo para esta sección', 'startravel' ),
    'id'         => $prefix . 'bullets2_main_title',
    'type'       => 'text'
) );

$group_field_id = $cmb_hero_bullets2->add_field( array(
    'id'          => $prefix . 'bullets2_list_group',
    'name'       => esc_html__( 'Listado de Items', 'startravel' ),
    'description' => __( 'Gropo de items en lista no ordenada', 'startravel' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Item {#}', 'startravel' ),
        'add_button'        => __( 'Agregar otro item', 'startravel' ),
        'remove_button'     => __( 'Remover Item', 'startravel' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro de remover este item?', 'startravel' )
    )
) );

$cmb_hero_bullets2->add_group_field( $group_field_id, array(
    'id'        => 'content',
    'name'      => esc_html__( 'Contenido del Item', 'startravel' ),
    'desc'      => esc_html__( 'Ingrese el contenido para este item', 'startravel' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$cmb_hero_bullets2->add_field( array(
    'name'       => esc_html__( 'Texto del Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un texto para el Boton Principal', 'startravel' ),
    'id'         => $prefix . 'bullets2_btn_text',
    'type'       => 'text'
) );

$cmb_hero_bullets2->add_field( array(
    'name'       => esc_html__( 'Link URL del Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un link o una URL para el Boton Principal', 'startravel' ),
    'id'         => $prefix . 'bullets2_btn_link',
    'type'       => 'text_url'
) );

$cmb_hero_bullets2->add_field( array(
    'name'       => esc_html__( 'Texto Posterior al Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un texto ubicado luego del Boton Principal', 'startravel' ),
    'id'         => $prefix . 'bullets2_post_btn_text',
    'type'       => 'text'
) );

/* --------------------------------------------------------------
    7.- SERVICIOS
-------------------------------------------------------------- */
$cmb_hero_services = new_cmb2_box( array(
    'id'            => $prefix . 'main_landing_services_metabox',
    'title'         => esc_html__( '7.- Servicios', 'startravel' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-ebook.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_hero_services->add_field( array(
    'name'       => esc_html__( 'Título de la Sección', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un título descriptivo para esta sección', 'startravel' ),
    'id'         => $prefix . 'services_main_title',
    'type'       => 'text'
) );

$cmb_hero_services->add_field( array(
    'name'       => esc_html__( 'Subtítulo de la Sección', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un subtítulo descriptivo para esta sección', 'startravel' ),
    'id'         => $prefix . 'services_subtitle',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$group_field_id = $cmb_hero_services->add_field( array(
    'id'          => $prefix . 'services_list_group',
    'name'       => esc_html__( 'Listado de Servicios', 'startravel' ),
    'description' => __( 'Gropo de Servicios en lista no ordenada', 'startravel' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Servicio {#}', 'startravel' ),
        'add_button'        => __( 'Agregar otro Servicio', 'startravel' ),
        'remove_button'     => __( 'Remover Servicio', 'startravel' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro de remover este Servicio?', 'startravel' )
    )
) );

$cmb_hero_services->add_group_field( $group_field_id, array(
    'id'         => 'icon',
    'name'      => esc_html__( 'Ícono del Servicio', 'startravel' ),
    'desc'      => esc_html__( 'Cargue un Ícono para este servicio', 'startravel' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Ícono', 'startravel' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_hero_services->add_group_field( $group_field_id, array(
    'id'         => 'title',
    'name'       => esc_html__( 'Titulo del Servicio', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un Titulo para el Boton Principal', 'startravel' ),
    'type'       => 'text'
) );

$cmb_hero_services->add_group_field( $group_field_id, array(
    'id'        => 'content',
    'name'      => esc_html__( 'Contenido del Servicio', 'startravel' ),
    'desc'      => esc_html__( 'Ingrese el contenido para este Servicio', 'startravel' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$cmb_hero_services->add_field( array(
    'name'       => esc_html__( 'Texto del Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un texto para el Boton Principal', 'startravel' ),
    'id'         => $prefix . 'services_btn_text',
    'type'       => 'text'
) );

$cmb_hero_services->add_field( array(
    'name'       => esc_html__( 'Link URL del Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un link o una URL para el Boton Principal', 'startravel' ),
    'id'         => $prefix . 'services_btn_link',
    'type'       => 'text_url'
) );

$cmb_hero_services->add_field( array(
    'name'       => esc_html__( 'Texto Posterior al Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un texto ubicado luego del Boton Principal', 'startravel' ),
    'id'         => $prefix . 'services_post_btn_text',
    'type'       => 'text'
) );

/* --------------------------------------------------------------
    8.- TESTIMONIOS
-------------------------------------------------------------- */
$cmb_hero_test = new_cmb2_box( array(
    'id'            => $prefix . 'main_landing_test_metabox',
    'title'         => esc_html__( '8.- Testimonios', 'startravel' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-ebook.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_hero_test->add_field( array(
    'name'       => esc_html__( 'Título de la Sección', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un título descriptivo para esta sección', 'startravel' ),
    'id'         => $prefix . 'test_main_title',
    'type'       => 'text'
) );

$cmb_hero_test->add_field( array(
    'name'       => esc_html__( 'Subtexto de la Sección', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un subtexto descriptivo para esta sección', 'startravel' ),
    'id'         => $prefix . 'test_subtext',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$cmb_hero_test->add_field( array(
    'name'       => esc_html__( 'Texto del Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un texto para el Boton Principal', 'startravel' ),
    'id'         => $prefix . 'test_btn_text',
    'type'       => 'text'
) );

$cmb_hero_test->add_field( array(
    'name'       => esc_html__( 'Link URL del Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un link o una URL para el Boton Principal', 'startravel' ),
    'id'         => $prefix . 'test_btn_link',
    'type'       => 'text_url'
) );

$cmb_hero_test->add_field( array(
    'name'       => esc_html__( 'Texto Posterior al Botón', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un texto ubicado luego del Boton Principal', 'startravel' ),
    'id'         => $prefix . 'test_post_btn_text',
    'type'       => 'text'
) );

$group_field_id = $cmb_hero_test->add_field( array(
    'id'          => $prefix . 'test_list_group',
    'name'       => esc_html__( 'Listado de Testimonios', 'startravel' ),
    'description' => __( 'Gropo de Testimonios en lista no ordenada', 'startravel' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Testimonio {#}', 'startravel' ),
        'add_button'        => __( 'Agregar otro Testimonio', 'startravel' ),
        'remove_button'     => __( 'Remover Testimonio', 'startravel' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro de remover este item?', 'startravel' )
    )
) );

$cmb_hero_test->add_group_field( $group_field_id, array(
    'id'         => 'image',
    'name'      => esc_html__( 'Imagen del Testimonio', 'startravel' ),
    'desc'      => esc_html__( 'Cargue una imagen descriptiva para esta Testimonio', 'startravel' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'startravel' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_hero_test->add_group_field( $group_field_id, array(
    'id'        => 'video',
    'name'      => esc_html__( 'Video del Testimonio', 'startravel' ),
    'desc'      => esc_html__( 'Ingrese el url del Video para este Testimonio', 'startravel' ),
    'type' => 'text'
) );

$cmb_hero_test->add_group_field( $group_field_id, array(
    'id'        => 'content',
    'name'      => esc_html__( 'Contenido del Testimonio', 'startravel' ),
    'desc'      => esc_html__( 'Ingrese el contenido para este Testimonio', 'startravel' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$cmb_hero_test->add_group_field( $group_field_id, array(
    'id'        => 'author',
    'name'      => esc_html__( 'Autor del Testimonio', 'startravel' ),
    'desc'      => esc_html__( 'Ingrese el contenido para este Testimonio', 'startravel' ),
    'type' => 'text'
) );

/* --------------------------------------------------------------
    9.- FAQ
-------------------------------------------------------------- */
$cmb_hero_faq = new_cmb2_box( array(
    'id'            => $prefix . 'main_landing_faq_metabox',
    'title'         => esc_html__( '9.- Preguntas Frecuentes', 'startravel' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-ebook.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_hero_faq->add_field( array(
    'name'      => esc_html__( 'Imagen de Fondo de la Sección', 'startravel' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para esta sección', 'startravel' ),
    'id'         => $prefix . 'faq_bg_image',
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'startravel' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_hero_faq->add_field( array(
    'name'       => esc_html__( 'Título de la Sección', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un título descriptivo para esta sección', 'startravel' ),
    'id'         => $prefix . 'faq_main_title',
    'type'       => 'text'
) );

$cmb_hero_faq->add_field( array(
    'name'       => esc_html__( 'Subtexto de la Sección', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un subtexto descriptivo para esta sección', 'startravel' ),
    'id'         => $prefix . 'faq_main_subtitle',
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$group_field_id = $cmb_hero_faq->add_field( array(
    'id'          => $prefix . 'faq_list_group',
    'name'       => esc_html__( 'Listado de Preguntas', 'startravel' ),
    'description' => __( 'Gropo de Preguntas en lista no ordenada', 'startravel' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Pregunta {#}', 'startravel' ),
        'add_button'        => __( 'Agregar otra Pregunta', 'startravel' ),
        'remove_button'     => __( 'Remover Pregunta', 'startravel' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro de remover esta Pregunta?', 'startravel' )
    )
) );

$cmb_hero_faq->add_group_field( $group_field_id, array(
    'id'         => 'title',
    'name'       => esc_html__( 'Titulo de la Pregunta', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese un Titulo para esta Pregunta', 'startravel' ),
    'type'       => 'text'
) );

$cmb_hero_faq->add_group_field( $group_field_id, array(
    'id'        => 'content',
    'name'      => esc_html__( 'Contenido de la Pregunta', 'startravel' ),
    'desc'      => esc_html__( 'Ingrese el contenido para esta Pregunta', 'startravel' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$cmb_embed_ebook = new_cmb2_box( array(
    'id'            => $prefix . 'main_landing_modal_metabox',
    'title'         => esc_html__( '10.- Modal', 'startravel' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-ebook.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_embed_ebook->add_field( array(
    'id'         => $prefix . 'modal_content',
    'name'      => esc_html__( 'Contenido del Modal', 'startravel' ),
    'desc'      => esc_html__( 'Ingrese el contenido del Modal para este landing', 'startravel' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 1),
        'teeny' => false
    )
) );

$cmb_embed_ebook->add_field( array(
    'id'         => $prefix . 'modal_list_id',
    'name'       => esc_html__( 'ID de la lista en Sendinblue', 'startravel' ),
    'desc'       => esc_html__( 'Ingrese el ID de la lista en Sendinblue', 'startravel' ),
    'type'       => 'text'
) );