var formThanks = document.getElementById('formSubscribe'),
    formThanksBtn = document.getElementById('formSubscribeSubmit'),
    formModal = document.getElementById('formModal'),
    formModalBtn = document.getElementById('formModalSubmit'),
    validForm = true;

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

/* CUSTOM ON LOAD FUNCTIONS */
function formDocumentCustomLoad() {
    "use strict";
    console.log('Form Functions Correctly Loaded');

    if (formThanks) {
        formThanksBtn.addEventListener('click', function(e) {
            e.preventDefault();
            validForm = true;
            var elements = document.getElementsByClassName('form-email-input');
            var errorText = document.getElementById('errorEmail');
            if (elements[0].value == '') {
                errorText.classList.remove('d-none');
                validForm = false;
            } else {
                if (validateEmail(elements[0].value) == false) {
                    errorText.classList.remove('d-none');
                    validForm = false;
                } else {
                    errorText.classList.add('d-none');
                    validForm = true;
                }
            }

            if (validForm == true) {
                submitForm(validForm);
            }
        });
    }

    if (formModal) {
        formModalBtn.addEventListener('click', function(e) {
            e.preventDefault();
            validForm = true;
            var elements = document.getElementById('inputName');
            var errorText = document.getElementsByClassName('error-name');
            if (elements.value == '') {
                errorText[0].classList.remove('d-none');
                errorText[0].innerHTML = custom_admin_url.error_nombre;
                validForm = false;
            } else {
                if (elements.value == '') {
                    errorText[0].classList.remove('d-none');
                    errorText[0].innerHTML = custom_admin_url.invalid_nombre;
                    validForm = false;
                } else {
                    errorText[0].classList.add('d-none');
                    errorText[0].innerHTML = '';
                    validForm = true;
                }
            }

            var elements = document.getElementById('inputEmail');
            var errorText = document.getElementsByClassName('error-email');
            if (elements.value == '') {
                errorText[0].classList.remove('d-none');
                errorText[0].innerHTML = custom_admin_url.error_email;
                validForm = false;
            } else {
                if (validateEmail(elements.value) == false) {
                    errorText[0].classList.remove('d-none');
                    errorText[0].innerHTML = custom_admin_url.invalid_email;
                    validForm = false;
                } else {
                    errorText[0].classList.add('d-none');
                    errorText[0].innerHTML = '';
                    validForm = true;
                }
            }

            if (validForm == true) {
                submitFullForm();
            }
        });
    }
}

document.addEventListener("DOMContentLoaded", formDocumentCustomLoad, false);

function submitForm() {
    var emailForm = document.getElementsByClassName('form-email-input');
    var info = 'action=subscribe_contact&email_contact=' + emailForm[0].value;
    var elements = document.getElementsByClassName('loader-css');
    elements[0].classList.toggle("d-none");
    /* SEND AJAX */
    newRequest = new XMLHttpRequest();
    newRequest.open('POST', custom_admin_url.ajax_url, true);
    newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    newRequest.onload = function() {
        window.location = custom_admin_url.thanks_url;
    };
    newRequest.send(info);
}

function submitFullForm() {
    var emailForm = document.getElementById('inputEmail');
    var nameForm = document.getElementById('inputName');
    var listForm = document.getElementById('listID');
    var info = 'action=subscribe_contact_name&email_contact=' + emailForm.value + '&email_name=' + nameForm.value + '&list_id=' + listForm.value;
    var elements = document.getElementsByClassName('loader-css');
    elements[0].classList.toggle("d-none");
    /* SEND AJAX */
    newRequest = new XMLHttpRequest();
    newRequest.open('POST', custom_admin_url.ajax_url, true);
    newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    newRequest.onload = function() {
        window.location = custom_admin_url.thanks_url;
    };
    newRequest.send(info);
}